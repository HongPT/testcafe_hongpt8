const { Selector } = require("testcafe");
class Login {
  constructor() {
    this.btnNoAccount = Selector("input.btn-link");
  }

  async clickNoAcc() {
    await testController.click(this.btnNoAccount);
  }
}
module.exports = new Login();
