const { Selector } = require("testcafe");
class emergency {
  constructor() {
    this.FristName = Selector("#input-given-name");
    this.LastName = Selector("#input-family-name");
    this.Phone = Selector("input[placeholder='Phone Number']");
    this.Btn = Selector("button[type='button']");
  }
  async inputEmer(first, last, phone) {
    await testController
      .typeText(this.FristName, first)
      .wait(2000)
      .typeText(this.LastName, last)
      .typeText(this.Phone, phone)
      .wait(2000);
  }

  async click() {
    await testController.click(this.Btn).wait(2000);
  }
}
module.exports = new emergency();
