const { Selector } = require("testcafe");

class SignUp {
  constructor() {
    this.txtEmail = Selector("#input-email");
    this.txtPhone = Selector("input[placeholder='Phone Number']");
    this.txtPass = Selector("#input-password");
    this.txtConfilmPass = Selector("#input-password_confirm");
    this.checkbox = Selector(".checkbox-checkmark");
    this.btn = Selector("button").withText("Get started");
  }

  async inputAccount(email, phone, pass, confilmpass) {
    await testController
      .typeText(this.txtEmail, email.trim())
      .typeText(this.txtPhone, phone)
      .typeText(this.txtPass, pass)
      .typeText(this.txtConfilmPass, confilmpass);
  }
  async Click() {
    await testController.click(this.checkbox).click(this.btn);
  }
}
module.exports = new SignUp();
