const { Selector } = require("testcafe");

class select_source {
  constructor() {
    this.Face = Selector("button").withText("Facebook/Instagram");
    this.Post = Selector("button:nth-child(2)");
    this.No = Selector("button:nth-child(2)");
  }
  async Click() {
    await testController.click(this.Face);
  }
}
module.exports = new select_source();
