const { Selector } = require("testcafe");
class select_Plan {
  constructor() {
    this.View = Selector(".styled__ViewMorePlans-sc-1u7ffq8-1.kjQOQt");
    this.Start = Selector(
      "div[class='styled__Plan-sc-ptg3zf-13 dqTJHn undefined medication'] button[class='Button-sc-9ihmqa-0 kGnTpc']"
    );
  }
  async Click() {
    await testController.click(this.View).wait(2000).click(this.Start);
  }
}
module.exports = new select_Plan();
