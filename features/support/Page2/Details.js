const { Selector } = require("testcafe");
class Details {
  constructor() {
    this.btnNever = Selector("button").withText("Never");
    this.btnRareley = Selector("button").withText("Rarely");
    this.btnSometime = Selector("button").withText("Sometimes");
    this.btnOften = Selector("button").withText("Often");
    this.btnVery = Selector("button").withText("Very Often");
    this.btnNever2 = Selector("button").withText("Never");
    this.btnRareley2 = Selector("button").withText("Rarely");
    this.btnOften2 = Selector("button").withText("Often");
    this.btnSometime2 = Selector("button").withText("Sometimes");
    this.btnSometime3 = Selector("button").withText("Sometimes");
    this.btnVery2 = Selector("button").withText("Very Often");
    this.btnVery3 = Selector("button").withText("Very Often");
    this.btnVery4 = Selector("button").withText("Very Often");
    this.btnSometime5 = Selector("button").withText("Sometimes");
    this.btnSometime6 = Selector("button").withText("Sometimes");
    this.btnVery5 = Selector("button").withText("Very Often");
    this.btnVery6 = Selector("button").withText("Very Often");
    this.btnVery7 = Selector("button").withText("Very Often");
    this.btnContinues = Selector("button").withText("Continue");
  }

  async click() {
    await testController
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .click(this.btnNever)
      .wait(2000)
      .click(this.btnContinues);
  }
}
module.exports = new Details();
