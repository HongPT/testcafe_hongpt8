const { Selector } = require("testcafe");
class Alcohol {
  constructor() {
    this.Event = Selector(
      "body > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(4) > label:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(2)"
    );
  }

  async ClickButton(text) {
    await testController.click(Selector("button").withText(text)).wait(2000);
  }

  async click() {
    await testController.click(this.Event).wait(2000);
  }
}
module.exports = new Alcohol();
