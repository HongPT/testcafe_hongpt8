const { Selector } = require("testcafe");
class Checkout {
  constructor() {
    this.btnNext = Selector("button[type='button']");
  }
  //click : k co tai khoan
  async click() {
    await testController.click(this.btnNext).wait(2000);
  }
}
module.exports = new Checkout();
