const { Selector } = require("testcafe");
class Question_bank {
  constructor() {
    this.btnNext1 = Selector(
      "button[class='NextButton-sc-99fbsg-0 dYURuU'] span"
    );
    this.btnNext2 = Selector(".NextButton-sc-99fbsg-0.dYURuU");
    this.btnNext3 = Selector(".NextButton-sc-99fbsg-0.dYURuU");
  }
  //click : k co tai khoan
  async clickNext() {
    await testController
      .click(this.btnNext1)
      .wait(2000)
      .click(this.btnNext2)
      .wait(2000)
      .click(this.btnNext3);
  }
}
module.exports = new Question_bank();
