const { Selector } = require("testcafe");
class Service_line {
  constructor() {
    this.CboADHD = Selector(
      "body > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > label:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(2)"
    );
    this.CboAlcohol = Selector(
      "body > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > label:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(2)"
    );
    this.btnConfilm = Selector(".SubmitButton-sc-1sdhfzr-0.eoOhsN");
  }
  async Click() {
    await testController
      .click(this.CboADHD)
      .wait(2000)
      .click(this.CboAlcohol)
      .wait(2000)
      .click(this.btnConfilm)
      .wait(2000);
  }
}
module.exports = new Service_line();
