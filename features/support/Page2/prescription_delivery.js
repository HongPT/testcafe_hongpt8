const { Selector } = require("testcafe");
class prescription_delivery {
  constructor() {
    this.Btn = Selector("button[type='delivery'] span");
    this.input = Selector("input[placeholder='Pharmacy Name or Address']");
    this.Serch = Selector("button[type='button']");
    this.CBO = Selector(
      "body > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(7) > div:nth-child(1) > label:nth-child(1) > span:nth-child(2)"
    );
  }

  async ClickToMyHome() {
    await testController.click(this.Btn);
  }
  async inputLocation(inputLocation) {
    await testController.typeText(this.input, inputLocation).wait(2000);
  }
  async ClickSearch() {
    await testController.click(this.Serch).click(this.CBO);
  }
}
module.exports = new prescription_delivery();
