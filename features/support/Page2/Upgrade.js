const { Selector } = require("testcafe");
class Upgrade {
  constructor() {
    this.btnCurrent = Selector(".TextButton-sc-1jjbkn7-0.hMUvtz");
    this.btnCurrent2 = Selector(".TextButton-sc-1jjbkn7-0.hMUvtz");
  }
  //click : k co tai khoan
  async click() {
    await testController
      .click(this.btnCurrent)
      .wait(2000)
      .click(this.btnCurrent2)
      .wait(2000);
  }
}
module.exports = new Upgrade();
