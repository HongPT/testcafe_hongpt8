const { Selector } = require("testcafe");
// exports.elements = {
//   txtLine1: function () {
//     return Selector("#input-address-line1").with({
//       boundTestRun: testController,
//     });
//   },
//   txtLine2: function () {
//     return Selector("#input-address-line2").with({
//       boundTestRun: testController,
//     });
//   },
//   txtCity: function () {
//     return Selector("#input-city").with({
//       boundTestRun: testController,
//     });
//   },
// };
class shipping {
  constructor() {
    this.Line1 = Selector("#input-address-line1");
    this.Line2 = Selector("#input-address-line2");
    this.City = Selector("#input-city");

    this.Zip = Selector("#input-postal-code");
    this.btn = Selector("button[type='button']");
  }
  async input(line1, line2, city) {
    await testController
      .typeText(this.Line1, line1)
      .typeText(this.Line2, line2)
      .typeText(this.City, city);
  }
  async slectOption() {
    const State = Selector("#input-region");
    const cityOption = State.find("option");
    await testController.click(State).click(cityOption.withText("Alaska"));
  }
  async inputZip(zip) {
    await testController.typeText(this.Zip, zip);
  }
  async Click() {
    await testController.click(this.btn);
  }
  // test(`Select an option from the drop-down menu`, async t => {
  //   const citySelect = Selector('#city');
  // const cityOption = citySelect.find('option');
  //     await t
  //         .click(citySelect)
  //         .click(cityOption.withText('London'))
  //         .expect(citySelect.value).eql('London');
  // });
}
module.exports = new shipping();
