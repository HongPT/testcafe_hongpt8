const { Selector } = require("testcafe");

class question_Bank2 {
  constructor() {
    this.btnNext2 = Selector(".NextButton-sc-99fbsg-0.dYURuU");
  }
  async Click() {
    await testController.click(this.btnNext2);
  }
}
module.exports = new question_Bank2();

// exports.elements = {
//   btnNext2: function () {
//     return Selector(".NextButton-sc-99fbsg-0.dYURuU").with({
//       boundTestRun: testController,
//     });
//   },
// };
