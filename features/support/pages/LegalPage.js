const { Selector } = require("testcafe");

class LegalPage {
  constructor() {
    this.FristName = Selector("#input-given-name");
    this.LastName = Selector("#input-family-name");
    this.ZipCode = Selector("#input-postal-code");
    this.Date = Selector("#input-dob");
    this.btnCountinue = Selector(".SubmitButton-sc-1sdhfzr-0.JOKWQ");
  }
  async input(firstName, lastName, code, date) {
    await testController
      .typeText(this.FristName, firstName)
      .typeText(this.LastName, lastName)
      .typeText(this.ZipCode, code)
      .typeText(this.Date, date);
  }

  async Click() {
    await testController.click(this.btnCountinue);
  }
}
module.exports = new LegalPage();
