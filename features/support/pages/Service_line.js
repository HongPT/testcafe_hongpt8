const { Selector } = require("testcafe");
class Service_line {
  constructor() {
    this.CboAnxiety = Selector("span.checkbox-checkmark");
    this.btnConfilm = Selector(".SubmitButton-sc-1sdhfzr-0.eoOhsN");
  }
  async Click() {
    await testController.click(this.CboAnxiety).click(this.btnConfilm);
  }
}
module.exports = new Service_line();

// exports.elements = {
//   CboAnxiety: function () {
//     return Selector("span.checkbox-checkmark").with({
//       boundTestRun: testController,
//     });
//   },

//   btnConfilm: function () {
//     return Selector(".SubmitButton-sc-1sdhfzr-0.eoOhsN").with({
//       boundTestRun: testController,
//     });
//   },
// };
