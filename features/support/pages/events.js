const { Selector } = require("testcafe");
// exports.elements = {
//   clickEvent: function () {
//     return Selector(
//       "body > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(4) > label:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(2)"
//     ).with({
//       boundTestRun: testController,
//     });
//   },

//   clickConfilm: function () {
//     return Selector(".SubmitButton-sc-1sdhfzr-0.eoOhsN").with({
//       boundTestRun: testController,
//     });
//   },
// };
class events {
  constructor() {
    this.Event = Selector(
      "body > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(4) > label:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(2)"
    );
    this.Confilm = Selector(".SubmitButton-sc-1sdhfzr-0.eoOhsN");
  }
  async Click() {
    await testController.click(this.Event).click(this.Confilm);
  }
}
module.exports = new events();
