const { Selector } = require("testcafe");
class question_Bank {
  constructor() {
    this.btnNext = Selector(
      "button[class='NextButton-sc-99fbsg-0 dYURuU'] span"
    );
  }
  async Click() {
    await testController.click(this.btnNext);
  }
}
module.exports = new question_Bank();
// exports.elements = {
//   btnNext: function () {
//     return Selector("button[class='NextButton-sc-99fbsg-0 dYURuU'] span").with({
//       boundTestRun: testController,
//     });
//   },
// };
