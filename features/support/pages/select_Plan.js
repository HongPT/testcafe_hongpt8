const { Selector } = require("testcafe");
// exports.elements = {
//   clickView: function () {
//     return Selector(".styled__ViewMorePlans-sc-1u7ffq8-1.kjQOQt").with({
//       boundTestRun: testController,
//     });
//   },

//   clickStart: function () {
//     return Selector(
//       "div[class='styled__Plan-sc-ptg3zf-13 fsFaxn undefined medication'] button[class='Button-sc-9ihmqa-0 kGnTpc']"
//     ).with({
//       boundTestRun: testController,
//     });
//   },
// };

class select_Plan {
  constructor() {
    this.View = Selector(".styled__ViewMorePlans-sc-1u7ffq8-1.kjQOQt");
    // this.Start = Selector(
    //   "div[class='styled__Plan-sc-ptg3zf-13 fsFaxn undefined medication'] button[class='Button-sc-9ihmqa-0 kGnTpc']"
    // );
    this.Start = Selector("button").withText("Start Today");
  }
  async ClickView() {
    await testController.click(this.View);
  }
  async ClickStart() {
    await testController.click(this.Start);
  }
}
module.exports = new select_Plan();
