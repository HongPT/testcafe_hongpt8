// const { Selector } = require("testcafe");
// exports.elements = {
//   clickFace: function () {
//     return Selector("button").withText("Facebook/Instagram");
//   },
// };

// function select(selector) {
//   return Selector(selector).with({ boundTestRun: testController });
// }
// exports.select_source = {
//   clickFace: function () {
//     return Selector("button").withText("Facebook/Instagram");
//   },
// };
const { Selector } = require("testcafe");
// exports.elements = {
//   clickFace: function () {
//     return Selector("button:nth-child(3)").with({
//       boundTestRun: testController,
//     });
//   },
//   clickPost: function () {
//     return Selector("button:nth-child(2)").with({
//       boundTestRun: testController,
//     });
//   },
//   clickNo: function () {
//     return Selector("button:nth-child(2)").with({
//       boundTestRun: testController,
//     });
//   },
// };

class select_source {
  constructor() {
    this.Face = Selector("button:nth-child(3)");
    this.Post = Selector("button:nth-child(2)");
    this.No = Selector("button:nth-child(2)");
  }
  async Click() {
    await testController.click(this.Post).click(this.No);
  }
}
module.exports = new select_source();
