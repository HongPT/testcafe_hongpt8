const { Selector } = require("testcafe");
exports.elements = {
  txtName: function () {
    return Selector("#fullName");
  },

  txtCardNumber: function () {
    return Selector("input").withAttribute("name", "cardnumber").with({
      boundTestRun: testController,
    });
  },
  txtDate: function () {
    return Selector("input").withAttribute("name", "exp-date");
  },
  txtcvc: function () {
    return Selector("input").withAttribute("name", "cvc");
  },

  txtZip: function () {
    return Selector("input[placeholder='ZIP']");
  },

  frame1: function () {
    return Selector("iframe").nth(0);
  },

  frame2: function () {
    return Selector("iframe").nth(1);
  },
  frame3: function () {
    return Selector("iframe").nth(2);
  },

  btnSubmit: function () {
    return Selector(
      "button[class='styled_components__SubmitButton-sc-1b0fzdr-5 kJMcgt']"
    ).with({
      boundTestRun: testController,
    });
  },
};
