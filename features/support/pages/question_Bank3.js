const { Selector } = require("testcafe");

class question_Bank3 {
  constructor() {
    this.btnNext3 = Selector(
      "button[class='NextButton-sc-99fbsg-0 dYURuU'] span"
    );
  }
  async Click() {
    await testController.click(this.btnNext3);
  }
}
module.exports = new question_Bank3();
// exports.elements = {
//   btnNext3: function () {
//     return Selector("button[class='NextButton-sc-99fbsg-0 dYURuU'] span").with({
//       boundTestRun: testController,
//     });
//   },
// };
