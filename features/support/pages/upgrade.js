const { Selector } = require("testcafe");
// exports.elements = {
//   clickContinues: function () {
//     return Selector(".TextButton-sc-1jjbkn7-0.hMUvtz").with({
//       boundTestRun: testController,
//     });
//   },
// };
class upgrade {
  constructor() {
    this.Continues = Selector(".TextButton-sc-1jjbkn7-0.hMUvtz");
  }
  async Click() {
    await testController.click(this.Continues);
  }
}
module.exports = new upgrade();
