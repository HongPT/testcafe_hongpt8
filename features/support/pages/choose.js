const { Selector } = require("testcafe");

class choose {
  constructor() {
    this.btnNo = Selector("button:nth-child(2)");
  }
  async Click() {
    await testController.click(this.btnNo);
  }
}
module.exports = new choose();
