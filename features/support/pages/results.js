const { Selector } = require("testcafe");
class results {
  constructor() {
    this.btnContinues = Selector("#submitButton");
  }
  async Click() {
    await testController.click(this.btnContinues);
  }
}
module.exports = new results();
