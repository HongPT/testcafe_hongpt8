
Feature: NCI Page

    Scenario: NCI_1243

        Given the alcohol and Bipolar services are on in Texas state
        And I am a new user
        When I click on "I don't have an account."
        And complete account information step
        And completed personal information step
        And select "ADHD" + “Addiction” - ”Alcohol” service lines
        And answer a list of alcohol question
        And answer questions of ADHD
        Then I see Preliminary Screen Indicators screen showing my “Alcohol use“ level
        And see Preliminary Screen Indicators screen showing my “ADHD“ level
        When I click “Confirm“Then I see “Medication & Care Counseling“ plan
        When I click “VIEW MORE PLANS“
        Then I see “Medication & Care Counseling“, “Medication & Therapy“ and Therapy plans
        When I note down the price of “Medication & Care Counseling“ plan for first month
        And select “Medication & Care Counseling“ plan
        And I get navigated to Billing Information screen
        Then I see “Medication & Care Counseling“ which I chose
        And the price is which I noted down
        And the price for month later is $85
        When I complete check out
        And complete shipping information
        And answer medical assessments
        And input height and weight
        And answer health and history questions
        Then notice that there is no "How often do you drink" question
        When I complete “Identity Verification“
        When schedule appointment with prescriber
        And select Counselor preferences
        And schedule appointment with counselor
        And click “Continue to the client portal“
        Then I get navigated to Message box chat
        And see greeting message from Counselor
        When I open Invoice
        And click PDF to open
        Then I see the charge
