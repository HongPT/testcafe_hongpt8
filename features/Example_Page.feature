# Feature: Example Page User Details

#     Scenario: The one where user enters all the details on Example Page

#         Given that I am a client who is seeking for an online anxiety and alcohol cessation service that is available in my state
#         And I found a link that directs me to Cerebral website
#         And I navigate to login page from the landing page
#         When I click on "I don't have an account."
#         And complete account information step
#         And completed personal information step
#         And select “Anxiety“ service line
#         And select “Addiction” - “Alcohol“ service lines
#         And answer a list of questions regarding to anxiety
#         And answer a list of questions regarding to alcohol
#         Then I see Preliminary Screen Indicators screen showing my “Anxiety“ level and “Alcohol use“ level
#         When I click “Confirm“
#         Then I see “Medication & Care Counseling“ plan
#         When I click “VIEW MORE PLANS“
#         Then I see “Medication & Care Counseling“, “Medication & Therapy“, “Therapy“ plans
#         When I note down the first month price and discount price of “Medication & Care Counseling“ plan
#         And select “Medication & Care Counseling“ plan
#         And get navigated to Billing Information screen
#         Then I see “Medication & Care Counseling“ which I chose
#         And the price for first month is like I noted
#         # And the price for month later is $85
#         And the promo code is “CARE30“
#         When I complete check out
#         Then I see “Add Therapy to improve your care“ plan
#         When I click “Continue with current plan“ to skip
#         And complete shipping information
#         And complete delivery method
#         And complete post checkout questions
#         # And complete “Identity Verification“ (refer the upper note to work around)
#         And select “I prefer a video visit“
#         And schedule appointment with prescriber
#         And select Counselor preferences
#         And schedule appointment with counselor
#         And click “Continue to the client portal“
#         Then I get navigated to Message box chat
#         And see Welcome Message from care counselor
