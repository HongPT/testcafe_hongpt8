const { Given, When, Then } = require("cucumber");

const { Selector } = require("testcafe");

const Login = require("../support/Page2/Login");
const SignUp = require("../support/Page2/SignUp");
const CreateAcc = require("../support/Page2/CreateAcc");
const Service_line = require("../support/Page2/Service_line");
const Question_bank = require("../support/Page2/Question_bank");
const Details = require("../support/Page2/Details");
const Alcohol = require("../support/Page2/Alcohol");
const select_Plan = require("../support/Page2/select_Plan");
const payment = require("../support/Page2/payment");
const select_source = require("../support/Page2/select_source");
const upgrade = require("../support/Page2/Upgrade");
const shipping = require("../support/Page2/shipping");
const prescription_delivery = require("../support/Page2/prescription_delivery");
const Checkout = require("../support/Page2/Checkout");
const emergency = require("../support/Page2/emergency");

Given("the alcohol and Bipolar services are on in Texas state", function () {});
Given("I am a new user", async function () {
  await testController.navigateTo("https://app-fe-release.getcerebral.com/");
});

When("I click on {string}", async function (string) {
  await Login.clickNoAcc();
});

When("complete account information step", async function () {
  await SignUp.inputAccount(
    "phamong05@gmail.com",
    "1234567890",
    "Thucdo@gmail.com123",
    "Thucdo@gmail.com123"
  );
  await SignUp.Click();
  await testController.wait(2000);
});

When("completed personal information step", async function () {
  await CreateAcc.input(
    "Alcohol",
    "Anxiety Medcation Counseling",
    "11111",
    "05101990"
  );
  await CreateAcc.Click();
  await testController.wait(2000);
});

When("select {string} + “Addiction” - ”Alcohol” service lines", async function (
  string
) {
  await Service_line.Click();
  await testController.wait(2000);

  //   await Question_bank.clickNext();
  //   await testController.wait(2000);
});

When("answer a list of alcohol question", async function () {
  await Question_bank.clickNext();
  await testController.wait(2000);
  await Details.click();
  await testController.wait(2000);
});

When("answer questions of ADHD", function () {});

Then(
  "I see Preliminary Screen Indicators screen showing my “Alcohol use“ level",
  function () {}
);

Then(
  "see Preliminary Screen Indicators screen showing my “ADHD“ level",
  function () {}
);

When(
  "I click “Confirm“Then I see “Medication & Care Counseling“ plan",
  async function () {
    await Alcohol.ClickButton("4 or more times a week");
    await Alcohol.ClickButton("5-6");
    await Alcohol.ClickButton("Monthly");
    await Alcohol.ClickButton("Very difficult");
    await Alcohol.click();
    await Alcohol.ClickButton("Confirm");
    await Alcohol.ClickButton("Continue");
    await testController.wait(2000);
  }
);

When("I click “VIEW MORE PLANS“", async function () {
  await select_Plan.Click();
  await testController.wait(2000);
});

Then(
  "I see “Medication & Care Counseling“, “Medication & Therapy“ and Therapy plans",
  function () {}
);

When(
  "I note down the price of “Medication & Care Counseling“ plan for first month",
  function () {}
);

When("select “Medication & Care Counseling“ plan", function () {});

When("I get navigated to Billing Information screen", async function () {
  await testController.scroll("bottom").wait(2000);
  const txtName = payment.elements.txtName();
  await testController.typeText(txtName, "hhhhh").wait(2000);
  const frame1 = payment.elements.frame1();
  await testController.switchToIframe(frame1).wait(2000);
  const txtCard1 = payment.elements.txtCardNumber();
  await testController
    .typeText(txtCard1, "4242424242424242")
    .switchToMainWindow();
  const frame2 = payment.elements.frame2();
  await testController.switchToIframe(frame2);
  const txtDate = payment.elements.txtDate();
  await testController.typeText(txtDate, "1222").switchToMainWindow();
  const frame3 = payment.elements.frame3();
  await testController.switchToIframe(frame3);
  const txtcvc = payment.elements.txtcvc();
  await testController.typeText(txtcvc, "111").switchToMainWindow();
  const txtzip = payment.elements.txtZip();
  await testController.typeText(txtzip, "77089");
  const click = payment.elements.btnSubmit();
  await testController.click(click);
});

Then("I see “Medication & Care Counseling“ which I chose", function () {});

Then("the price is which I noted down", function () {});

Then("the price for month later is ${int}", function (int) {});

When("I complete check out", function () {});

When("complete shipping information", async function () {
  await testController.scroll("bottom").wait(2000);
  await upgrade.click();
  await select_source.Click();
  //
  await testController.scroll("bottom").wait(2000);
  await shipping.input("sfsfsd", "asfdsgfdh", "adsdgdfhf");
  await shipping.slectOption();
  await shipping.inputZip("77089");
  await shipping.Click();
});

When("answer medical assessments", async function () {
  await prescription_delivery.ClickToMyHome();
  await prescription_delivery.inputLocation("Ha Noi");
  await prescription_delivery.ClickSearch();
});

When("input height and weight", async function () {
  await Checkout.click();
  await emergency.inputEmer("gghdfh", "sfdhj", "1234567890");
  await emergency.click();
});

When("answer health and history questions", function () {});

Then("notice that there is no {string} question", function (string) {});

When("I complete “Identity Verification“", function () {});

When("schedule appointment with prescriber", function () {});

When("select Counselor preferences", function () {});

When("schedule appointment with counselor", function () {});

When("click “Continue to the client portal“", function () {});

Then("I get navigated to Message box chat", function () {});

Then("see greeting message from Counselor", function () {});

When("I open Invoice", function () {});

When("click PDF to open", function () {});

Then("I see the charge", function () {});
